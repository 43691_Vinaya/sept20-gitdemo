#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"
void owner_area(user_t *u)
{
  int choice;
  do
  {
      printf("\n\n0. Sign Out\n1. Appoint Librarian\n2. Edit Profile\n3. Change Password\n4. Fees/Fine Report\n5. Book Availability\n6. Book Categories/Subjects\nEnter choice: ");
      scanf("%d",&choice);
      switch(choice)
      {
          case 1://Appoint Librarian
                 appoint_librarian();
                break;
          case 2://Edit Profile
                  
                  edit_profile_by_email(u);
                break;
          case 3://Change Password
                  change_password(u);
                break;
          case 4:// Fees/Fine Report
                  

                break;     
          case 5://Book Availability
                break;
          case 6://Book Categories/Subjects
                break;                         
      }
  }while(choice!=0);
}

void appoint_librarian()
{
    //add details of librarian
    user_t u;
    user_accept(&u);
    //change user role to librarian
    strcpy(u.role,ROLE_LIBRARIAN);
    //add librarian into the user file
    user_add(&u);

}